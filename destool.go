package lljson

import (
	"reflect"
)

func newBeginDeserialize(v interface{}) *BeginDeserialize {
	return &BeginDeserialize{v: v}
}

// FieldMap 返回json串的key和val
func (b *BeginDeserialize) FieldMap() {

}

func (b *BeginDeserialize) TypeElem() reflect.Type {
	t := reflect.TypeOf(b.v).Elem()
	return t
}

func (b *BeginDeserialize) ValueElem() *reflect.Value {
	v := reflect.ValueOf(b.v).Elem()
	//fmt.Printf("1%p\n", &v)
	return &v
}

func (b *BeginDeserialize) IfStruct() bool {
	switch b.v.(type) {
	case *int, *uint, *uint16, *float64, *string, *bool:
		return false
	case **int, **uint, **float64, **string, **bool:
		panic("不支持指针切片指向非结构体")
	}
	return true
}

// HandleUnStruct 处理非结构体int, uint这些
func (b *BeginDeserialize) HandleUnStruct() error {
	inValueElem := reflect.ValueOf(b.v).Elem()
	byteValue := newByteValue(string2bytes(b.json))
	//fmt.Println("un", inValue.Type())
	switch inValueElem.Kind() {
	case reflect.String:
		inValueElem.SetString(byteValue.string())

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		inValueElem.SetInt(byteValue.int64())

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		inValueElem.SetUint(byteValue.uint())

	case reflect.Float64:
		inValueElem.SetFloat(byteValue.float64())

	case reflect.Bool:
		inValueElem.SetBool(byteValue.bool())
	}
	return nil
}

func (b *BeginDeserialize) reflectKind() reflect.Kind {
	return reflect.TypeOf(b.v).Kind()
}
