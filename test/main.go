package main

import (
	"encoding/json"
	"fmt"
	"lljson"
)

func main() {
	//XTest1()
	//FTest1()
	XFTest11()
}

func FTest1() {
	p := new(Person)
	lljson.Deserialize(Json1(), p)
	fmt.Println(p)
}

func XTest1() {
	serialize := lljson.Serialize(*type1Struct())
	marshal, err := json.Marshal(type1Struct())
	if err != nil {
		panic(err)
	}
	fmt.Println(serialize)
	fmt.Println(string(marshal))
	fmt.Println(serialize == string(marshal))

}

func XFTest11() {
	//对对象进行序列化
	serialize := lljson.Serialize(*type1Struct())
	p := new(Person)
	//反序列化
	lljson.Deserialize(serialize, p)
	//序列化
	s := lljson.Serialize(*p)
	fmt.Println(s)
	//让序列化的结果和序列化又反序列化后又序列化的结果进行比较
	fmt.Println("serialize == s?", serialize == s)
	//让序列化的结果和json.Marshal的结果进行比较
	marshal, _ := json.Marshal(p)
	fmt.Println("marshal == s?", bytes2string(marshal) == s)
	fmt.Println(p)
}
