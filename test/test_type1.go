package main

import "lljson"

type Money struct {
	Much     int
	Families int
	Floatt   float64
}

// Person 一个用于测试的结构
type Person struct {
	Money     Money
	Id        int16 `json:"id"`
	UserName  string
	Password  string
	Families  []Person
	Families2 []Person
	Families3 []*Person
	Uin       []uint16
	Ptr       *Money
	If        bool
	Ptr2      *int
}

func type1Struct() *Person {
	person := Person{
		Id:       111,
		UserName: "111",
		Password: "222",
		Families: []Person{
			{
				UserName: "los",
				Password: "123456",
				Money:    Money{Much: 22},
			},
			{
				UserName: "los2",
				Password: "fawbiufbw",
				Families: []Person{
					{
						Id:       2617,
						UserName: "dawd",
					},
				},
			},
		},
		Families2: []Person{
			{
				UserName: "Families2",
			},
		},
		Families3: []*Person{
			{
				UserName: "Families3",
			},
		},
		Uin:   []uint16{1, 2, 3},
		Money: Money{Much: 11, Floatt: 3.1415},
		Ptr:   &Money{Much: 456},
	}
	return &person
}

func Json1() string {
	return lljson.Serialize(*type1Struct())
}
