package lljson

import (
	"log"
	"strconv"
	"strings"
	"unsafe"
)

func string2bytes(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(&s))
}
func bytes2string(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// AddQuotationMarks 给一个字符串加上双引号
func AddQuotationMarks(s string) string {
	return "\"" + s + "\""
}

// AddBraceRight 给一个字符串后面加上}
func AddBraceRight(s string) string {
	return s + "}"
}

// AddComma 在字符串后添加逗号
func AddComma(s string) string {
	return s + ","
}

// ToString 返回类型对应的字符串
// TODO:添加类型
func ToString(v interface{}) string {
	switch v.(type) {
	case string:
		return AddQuotationMarks(v.(string))
	case int:
		return strconv.Itoa(v.(int))
	case int16:
		return strconv.Itoa(int(v.(int16)))
	case uint:
		return strconv.Itoa(int(v.(uint)))
	case uint16:
		return strconv.Itoa(int(v.(uint16)))
	case bool:
		if v.(bool) == true {
			return "true"
		} else {
			return "false"
		}
	case float64:
		return strconv.FormatFloat(v.(float64), 'f', -1, 64)
	}
	return ""
}

// DeleteAComma 在字符串末尾减去一个,
func DeleteAComma(s string) string {
	return strings.Trim(s, ",")
}

func (b ByteValue) string() string {
	return string(b)
}

// 有错直接抛panic
func (b ByteValue) int64() int64 {
	atoi, err := strconv.Atoi(string(b))
	if err != nil {
		panic(err)
	}
	return int64(atoi)
}

func (b ByteValue) uint() uint64 {
	atoi, err := strconv.Atoi(string(b))
	if err != nil {
		panic(err)
	}
	return uint64(atoi)
}

func (b ByteValue) float64() float64 {
	float, err := strconv.ParseFloat(string(b), 64)
	if err != nil {
		panic(err)
	}
	return float
}

func (b ByteValue) bool() bool {
	if string(b) == "true" {
		return true
	}
	return false
}

func (b ByteValue) sliceUint() []uint {
	bSlice := DivideSlice(b)
	uintSlice := make([]uint, 0)
	for _, v := range bSlice {
		s := string(v)
		atoi, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		uintSlice = append(uintSlice, uint(atoi))
	}
	return uintSlice
}

func (b ByteValue) sliceInt() []int {
	bSlice := DivideSlice(b)
	intSlice := make([]int, 0)
	for _, v := range bSlice {
		s := string(v)
		atoi, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		intSlice = append(intSlice, atoi)
	}
	return intSlice
}

func (b ByteValue) sliceFloat() []float64 {
	bSlice := DivideSlice(b)
	floatSlice := make([]float64, 0)
	for _, v := range bSlice {
		s := string(v)
		Float, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		floatSlice = append(floatSlice, Float)
	}
	return floatSlice
}

func (b ByteValue) sliceBool() []bool {
	bSlice := DivideSlice(b)
	boolSlice := make([]bool, 0)
	for _, v := range bSlice {
		s := string(v)
		if s == "true" {
			boolSlice = append(boolSlice, true)
		} else {
			boolSlice = append(boolSlice, false)
		}
	}
	return boolSlice
}

// ArrayType 切片是底层切片([]int, []uint)，还是结构体嵌套切片，是底层切片就返回true
func ArrayType(v interface{}) bool {
	switch v.(type) {
	case []uint:
		return true
	case []int:
		return true
	case []uint8:
		return true
	case []uint16:
		return true
	case []float64:
		return true
	case []bool:
		return true
	}
	return false
}

// rangeJsonToIndex 遍历到json一直到index，当{}级别为hua，[]级别为zhong的时候返回true
func rangeJsonToIndex(jsonS string, index, hua, zhong int) bool {
	up := 0
	min := 0
	for i, val := range []byte(jsonS) {
		if i == index {
			if up == hua && min == zhong {
				return true
			} else {
				return false
			}
		}
		switch val {
		case '{':
			up++
		case '}':
			up--
		case '[':
			min++
		case ']':
			min--
		}
	}
	log.Println("out of range")
	return false
}

func newByteValue(in []byte) ByteValue {
	return in
}
