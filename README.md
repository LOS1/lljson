# lljson

lljson是一个对结构体进行序列化和反序列化的框架

## 支持类型

- int
- uint
- float64
- string
- bool
- slice
- struct
- ptr
- (int8，16这种可以case时加上)实现以下四个方法的正常运作即可
  - (b *BeginDeserialize) IfStruct()
  -  ArrayType(v interface{}) bool 
  - (b *BeginSerialize) HandleBottomArray(v interface{})
  - ToString(v interface{}) string 


## 使用方法

```
git clone https://gitee.com/LOS1/lljson
```

#### 1.序列化

```go
type Money struct {
	Much int
}

// Person 一个用于测试的结构
type Person struct {
    Id       int `json:"id"`
	UserName string
	Password string
	Families []Person
    Families2 []*Person
	Uin      []uint
	Money    Money
	Ptr      *Money
	If       bool
	Ptr2     *int
}

func main() {
	person := Person{
		Id:       111,
		UserName: "111",
		Password: "222",
		Families: []Person{
			{
				UserName: "los",
				Password: "123456",
				Money:    Money{Much: 22},
			},
			{
				UserName: "los2",
				Password: "fawbiufbw",
				Families: []Person{
					{
						Id:       2617,
						UserName: "dawd",
					},
				},
			},
		},
		Uin:   []uint{1, 2, 3},
		Money: Money{Much: 11},
		Ptr:   &Money{Much: 456},
	}
    //这个person要是一个对象
    s := lljson.Serialize(person)
    
}
```

#### 2.反序列化

```go
type Money struct {
	Much int
}

// Person 一个用于测试的结构
type Person struct {
	Id       int
	UserName string
	Password string
	Families []Person
	Uin      []uint
	Money    Money
	Ptr      *Money
	If       bool
	Ptr2     *int
}

func main() {
	jsonS := xxxxxxx//这里填上标准的json字符串
    person := new(Person)
	Deserialize(jsonS, person)
	return
}
```

# 序列化

一个字段一个字段的递归

# 反序列化

### 方案1

用json对应的的字段去对应interface，但字符解析太麻烦了

### 方案2

对对象结构，一字段一字段的进行处理，拿到fieldname和或者json tag name，在json串里找，一种类型一种类型的制定反序列化规则

### 采用方案2
