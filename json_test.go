package lljson

import (
	"fmt"
	"testing"
)

type IntTest struct {
	Uint    uint
	Int     int
	Float64 float64
	String  string
	Bool    bool
}

func TestName(t *testing.T) {
	send := &IntTest{
		Uint:    1,
		Int:     2,
		Float64: 3.14,
		String:  "string",
	}
	serialize := Serialize(*send)
	fmt.Println(serialize)

	recv := new(IntTest)
	Deserialize(serialize, recv)
	fmt.Println(recv)
}

type SliceTest struct {
	IntSc     []int
	UintSc    []uint
	Float64SC []float64
	BoolSC    []bool
}

func TestFanXvLieHua(t *testing.T) {
	scTestNULL := &SliceTest{
		IntSc:     nil,
		UintSc:    nil,
		Float64SC: nil,
		BoolSC:    nil,
	}

	serialize := Serialize(*scTestNULL)
	fmt.Println(serialize)

	s := new(SliceTest)
	Deserialize(serialize, s)
	fmt.Println(s)

	scTest := &SliceTest{
		IntSc:     []int{1, 2, 3},
		UintSc:    []uint{4, 5, 6},
		Float64SC: []float64{7, 8, 9},
		BoolSC:    []bool{true, false},
	}

	serialize2 := Serialize(*scTest)
	fmt.Println(serialize2)

	s2 := new(SliceTest)
	Deserialize(serialize2, s2)
	fmt.Println(s2)
}

type InStruct struct {
	IntTest IntTest
}

type StructTest struct {
	IntTest  IntTest
	Ptr      *InStruct
	InStruct InStruct
}

func TestStruct(t *testing.T) {
	Struct := new(StructTest)
	Struct.InStruct.IntTest = IntTest{
		Uint:    1,
		Int:     2,
		Float64: 3.14,
		String:  "string",
	}
	Struct.IntTest = IntTest{
		Uint:    5,
		Int:     6,
		Float64: 1.11,
		String:  "test",
		Bool:    true,
	}

	serialize := Serialize(*Struct)
	fmt.Println(serialize)

	s := new(StructTest)
	Deserialize(serialize, s)
	fmt.Println(s)
}

//type InStruct struct {
//	IntTest IntTest
//}

type Random struct {
	Name *InStruct
}

type PtrTest struct {
	Name     string
	InStruct *InStruct
	//IntPtr   *int
	SelfPtr []*PtrTest
	Random  *Random
}

func TestPtr(t *testing.T) {
	//i := 1
	p := &PtrTest{
		Name: "PtrTest",
		InStruct: &InStruct{IntTest: IntTest{
			Uint:    1,
			Int:     2,
			Float64: 3.1415,
			String:  "test",
			Bool:    false,
		}},
		//IntPtr: &i,
		SelfPtr: []*PtrTest{
			{
				Name: "PtrTest",
			},
			{
				Name: "Ptr22",
			},
		},
		Random: &Random{Name: &InStruct{IntTest: IntTest{String: "Rawdwa"}}},
	}
	serialize := Serialize(*p)

	p2 := new(PtrTest)

	Deserialize(serialize, p2)
	fmt.Println(p2)
}
